import 'package:flutter/material.dart';

class LogOut extends StatefulWidget {
  LogOut({Key? key}) : super(key: key);

  @override
  State<LogOut> createState() => LogOutState();
}

class LogOutState extends State<LogOut> {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      backgroundColor: Colors.white,
      title: Text(
        "LOGOUT APLIKASI",
        style: TextStyle(color: Colors.black),
      ),
      titlePadding: EdgeInsets.all(8),
      content: Text("Apakah anda ingin LOGOUT ?"),
      actions: [
        MaterialButton(
          onPressed: () {
            print("TIDAK");
            Navigator.of(context).pop();
          },
          child: Text("TIDAK"),
          color: Colors.blue[200],
        ),
        MaterialButton(
          onPressed: () {
            print("YA");
            Navigator.of(context).pop();
          },
          child: Text("YA"),
          color: Colors.blue,
        )
      ],
    );
  }
}
