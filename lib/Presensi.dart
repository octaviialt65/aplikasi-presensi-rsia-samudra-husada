import 'package:flutter/material.dart';

class Presensi extends StatelessWidget {
  const Presensi({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Presensi Karyawan'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const <Widget>[
            Text('Presensi Karyawan RSIA SAMUDRA HUSADA'),
          ],
        ),
      ),
    );
  }
}
