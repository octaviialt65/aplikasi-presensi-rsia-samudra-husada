import 'package:flutter/material.dart';

class MyHeaderDrawer extends StatefulWidget {
  const MyHeaderDrawer({Key? key}) : super(key: key);

  @override
  _MyHeaderDrawerState createState() => _MyHeaderDrawerState();
}

class _MyHeaderDrawerState extends State<MyHeaderDrawer> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.blue[600],
      width: double.infinity,
      height: 200,
      padding: const EdgeInsets.only(top: 20.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          CircleAvatar(
            radius: 50.0,
            backgroundImage: AssetImage("images/foto.png"),
          ),
          Container(
            margin: const EdgeInsets.only(bottom: 0.0),
            height: 40.0,
            decoration: const BoxDecoration(
              shape: BoxShape.rectangle,
            ),
          ),
          const Text(
            "Selamat Datang!",
            style: TextStyle(color: Colors.white, fontSize: 12),
          ),
          const Text(
            "Dwi Susanti",
            style: TextStyle(color: Colors.white, fontSize: 15),
          ),
        ],
      ),
    );
  }
}
